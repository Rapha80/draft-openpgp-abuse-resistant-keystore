This repo is the source for [https://datatracker.ietf.org/doc/draft-dkg-openpgp-abuse-resistant-keystore/](draft-dkg-openpgp-abuse-resistant-keystore).

You might also be interested in [the "editor's copy"](https://dkg.gitlab.io/draft-openpgp-abuse-resistant-keystore/).

There is an issue tracker and a place for merge requests [on gitlab](https://gitlab.com/dkg/draft-openpgp-abuse-resistant-keystore/).
